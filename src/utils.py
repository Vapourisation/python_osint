from curl_cffi.requests import BrowserType
from random import choice

BROWSERS = [x.value for x in BrowserType]


def _random_browser() -> BrowserType:
    """Return a random browser from the curl-cffi"""
    return choice(BROWSERS)


ddg_result_nodes = {
    "link_node": "a",
    "link_attrs": {"class": "result__url"},
    "href": True,
    "title_node": "h2",
    "title_attrs": {"class": "result__title"},
    "description_node": "a",
    "description_attrs": {"class": "result__snippet"},
    "link_pattern": "//duckduckgo.com/l/?uddg="
}

brave_results_nodes = {
    "link_node": "a",
    "link_attrs": {"class": "h"},
    "href": True,
    "title_node": "div",
    "title_attrs": {"class": "url"},
    "description_node": "div",
    "description_attrs": {"class": "snippet-description"}
}

bing_results_nodes = {
    "link_node": "div",
    "link_attrs": {"class": "b_attribution"},
    "href": False,
    "title_node": "h2",
    "title_attrs": None,
    "description_node": "div",
    "description_attrs": {"class": "b_caption"}
}

google_results_nodes = {
    "link_node": "a",
    "link_attrs": None,
    "href": True,
    "title_node": "h3",
    "title_attrs": None,
    "description_node": "div",
    "description_attrs": {"style": "-webkit-line-clamp:2"}
}

