import logging
import math
import pandas as pd
import queue
import requests
import time
import urllib

from bs4 import BeautifulSoup
from concurrent.futures import ThreadPoolExecutor

from utils import (
    ddg_result_nodes,
    brave_results_nodes,
    bing_results_nodes,
    google_results_nodes
)

logger = logging.getLogger(__name__)


class Search:
    def __init__(
        self,
        headers=None,
        pages=1,
        proxies=None,
        timeout=10,
        workers=3,
        output_format="csv"
    ):

        self.queue = queue.Queue()
        self.session = requests.Session()
        self.workers = workers
        self.term = ''
        if headers:
            self.session.headers.update(headers)

        if proxies:
            self.session.proxies.update(proxies)

        self.output = pd.DataFrame(
            columns=["url", "title", "description", "browser"])
        self.output_rows = []
        self.userAgent = "(Mozilla/5.0 (Windows; U; Windows NT 6.0;en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6"
        self.session.headers.update({'User-Agent': self.userAgent})
        self.output_format = output_format
        self.output_name = f'results-{time.strftime("%Y%m%d-%H%M%S")}.{self.output_format}'

    def __del__(self):
        self.save_results()
        self.session.close()

    def go(self, term: str, browser: str):
        self.term = term

        if browser == '__all__':
            browsers = ['bing', 'brave', 'ddg', 'google']
        else:
            browsers = [browser]

        with ThreadPoolExecutor(max_workers=self.workers) as executor:
            executor.map(self.run_request, browsers)

    def run_request(self, browser: str):
        if browser == 'bing':
            return self.bing(self.term)
        elif browser == 'brave':
            return self.brave(self.term)
        elif browser == 'ddg':
            return self.ddg(self.term)
        elif browser == 'google':
            return self.google(self.term)
        else:
            logger.debug(f'Specified browser ({browser}) is not supported yet')

    def save_results(self):
        logger.info(
            "\n---------------------\nSavings results!\n---------------------\n")
        df = pd.concat(self.output_rows, ignore_index=True)
        if self.output_format == "json":
            df.to_json(self.output_name)
        elif self.output_format == "csv":
            df.to_csv(self.output_name)
        elif self.output_format == "xlsx":
            df.to_excel(self.output_name)
        else:
            raise ValueError("Output format must be 'json', 'xlsx' or 'csv'")

    # Helper methods
    # ---------------------------------------------------

    def parse_html(self, html: str) -> BeautifulSoup:
        soup = BeautifulSoup(html, 'html.parser')
        return soup

    def get_and_parse_results(self, url: str, node: str, node_class: str):
        resp = self.session.get(url)

        if resp.status_code == 200:
            soup = self.parse_html(resp.text)
            results = soup.find_all(node, attrs={"class": node_class})

            logger.debug(f'Results found: {len(results)}')
            logger.debug(f'Node: {node}, Node class: {node_class}')

            return results
        else:
            logger.debug(f'Response code: {resp.status_code}')

        return None

    def process_results(self, browser, results, num_results, **node_dict):
        processed = 0

        while processed < num_results and processed < len(results):
            result = results[processed]
            link = result.find(
                node_dict["link_node"],
                attrs=node_dict["link_attrs"],
                href=node_dict["href"]
            )
            title = result.find(
                node_dict["title_node"],
                attrs=node_dict["title_attrs"]
            )
            description = result.find(
                node_dict["description_node"],
                attrs=node_dict["description_attrs"]
            )

            if link:
                if node_dict.get("href", True):
                    if 'href' not in link:
                        logger.error(f'"href" not in: {link}')

            if link:
                # Bing links are redirects, but their actual link is stored as a string
                href = link["href"] if node_dict.get(
                    "href", True) else link.text
                if href and title:
                    df2 = pd.DataFrame(
                        [
                            [
                                self.clean_url(href, node_dict.get(
                                    "link_pattern", None)),
                                self.clean_text(title.text),
                                description.text if description else "No description",
                                browser
                            ]
                        ],
                        columns=self.output.columns
                    )
                    self.output_rows.append(df2)
                    logger.info(
                        f'Processed: {math.floor(((processed + 1)/num_results)*100)}%')
            processed += 1

    def clean_url(self, url, pattern=None):
        if pattern:
            return urllib.parse.unquote(url.replace(pattern, ""))

        return urllib.parse.unquote(url)

    def clean_text(self, text):
        return text.strip()

    def get_next_page(self, url):
        pass

    # Search engine methods
    # ---------------------------------------------------

    def ddg(self, term: str, lang='us-en', num_results=13):
        escaped_term = urllib.parse.quote_plus(term)
        result_block = self.get_and_parse_results(
            f'https://duckduckgo.com/html/?q={escaped_term}&l={lang}', 'div', 'result')

        if not result_block or len(result_block) == 0:
            logger.error(f'No results found for {term} on duckduckgo.com')
            return

        self.process_results("ddg", result_block,
                             num_results, **ddg_result_nodes)

    def brave(self, term: str, num_results=10):
        escaped_term = urllib.parse.quote_plus(term)
        result_block = self.get_and_parse_results(
            f'https://search.brave.com/search?q={escaped_term}', 'div', 'snippet')

        if not result_block or len(result_block) == 0:
            logger.error(f'No results found for {term} on brave.com')
            return

        self.process_results("brave", result_block,
                             num_results, **brave_results_nodes)

    def bing(self, term: str, num_results=10):
        escaped_term = urllib.parse.quote_plus(term)
        result_block = self.get_and_parse_results(
            f'https://www.bing.com/search?q={escaped_term}&rdr=1', 'li', 'b_algo')

        if not result_block or len(result_block) == 0:
            logger.error(f'No results found for {term} on bing.com')
            return

        self.process_results("bing", result_block,
                             num_results, **bing_results_nodes)

    def google(self, term: str, num_results=10):
        escaped_term = urllib.parse.quote_plus(term)
        result_block = self.get_and_parse_results(
            f'https://www.google.com/search?q={escaped_term}', 'div', 'g')

        if not result_block or len(result_block) == 0:
            logger.error(f'No results found for {term} on google.com')
            return

        self.process_results("google", result_block,
                             num_results, **google_results_nodes)
