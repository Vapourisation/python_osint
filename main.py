import argparse

from src.Search import Search


def main(term: str, engine="bing"):
    search = Search()
    search.go(term, engine)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="term_search",
        description="Quickly search different browsers for your term",
    )
    parser.add_argument('term')
    parser.add_argument(
        '-e',
        '--engine',
        help="""which search engine/s you wish to search.
        Currently supported are Bing, Brave, DuckDuckGo and Google.
        You can specify __all__ if you wish to search all engines."""
    )

    args = parser.parse_args()
    print(args.term, args.engine)
    main(args.term, args.engine if args.engine else None)
